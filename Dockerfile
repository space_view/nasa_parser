FROM python:3.10-slim

RUN apt-get update \
    && apt-get install --no-install-recommends -y curl make

ENV POETRY_VIRTUALENVS_CREATE=false \
    POETRY_VERSION=1.1.11 \
    YOUR_ENV=development

RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | python -
ENV PATH "/root/.local/bin:$PATH"

WORKDIR /app
COPY nasa_parser ./
RUN poetry install