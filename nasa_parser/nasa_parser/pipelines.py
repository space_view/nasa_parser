from dataclasses import asdict

import pymongo

from nasa_parser.amqp import connect, publish_message


class MongoPipeline:
    def __init__(self, settings):
        self.mongo_uri = settings.get('MONGO_URL')
        self.mongo_db = settings.get('MONGO_DATABASE')
        self.collection_name = settings.get('MONGO_COLLECTION_NAME')

        self.client = None
        self.db = None
        self.queue_connection, self.queue_channel = connect(settings)

    @classmethod
    def from_crawler(cls, crawler):
        return cls(settings=crawler.settings)

    def open_spider(self, spider):
        self.client = pymongo.MongoClient(self.mongo_uri)
        self.db = self.client[self.mongo_db]

    def close_spider(self, spider):
        self.client.close()
        self.queue_channel.close()

    def process_item(self, item, spider):
        data = asdict(item)
        result = self.db[self.collection_name].update_one(item.index, {'$set': data}, upsert=True)
        # Все новые материалы нужно закинуть в очередь
        if result.upserted_id:
            publish_message(self.queue_channel, data)
        return item
