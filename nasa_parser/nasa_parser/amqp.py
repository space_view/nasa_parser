import json

import pika


def connect(settings):
    credentials = pika.PlainCredentials(settings.get('RABBITMQ_LOGIN'), settings.get('RABBITMQ_PASSWORD'))
    parameters = pika.ConnectionParameters(
        settings.get('RABBITMQ_HOST'), settings.get('RABBITMQ_PORT'), settings.get('RABBITMQ_VHOST'), credentials
    )
    connection = pika.BlockingConnection(parameters)
    channel = connection.channel()
    channel.queue_declare('new_posts', durable=True)
    return connection, channel


def publish_message(channel, message):
    channel.basic_publish(exchange='', routing_key='new_posts', body=json.dumps(message))
