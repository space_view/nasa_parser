import scrapy
from scrapy import Request

from nasa_parser.items import PhotoItem

DOMAIN = 'https://mars.nasa.gov/'
START_URL = f'{DOMAIN}mars2020/multimedia/images/'


class PerseverancePhotosSpider(scrapy.Spider):
    name = 'perseverance_photos'
    start_urls = [START_URL]

    def parse_page(self, response):
        for page in response.css('li.slide a::attr(href)').getall():
            yield Request(DOMAIN + page[1:], self.parse_image_page)

    def parse_image_page(self, response):
        return PhotoItem(
            title=response.css('.article_title::text').get(),
            img_urls=[f'{DOMAIN}{url[1:]}' for url in response.css('div.downloads a::attr(href)').getall()],
            full_path=response.request.url,
            type='perseverance_rover',
            category=None,
        )

    def parse(self, response):
        for page in response.css('div.pagination-bottom a::attr(href)').getall():
            yield Request(START_URL + page[1:], self.parse_page)
