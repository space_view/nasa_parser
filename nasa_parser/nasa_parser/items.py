from dataclasses import dataclass
from typing import Literal, Optional


@dataclass
class PhotoItem:
    title: str
    full_path: str
    img_urls: list[str]
    type: Literal['perseverance_rover', 'curiosity', 'hubble']
    category: Optional[str]

    @property
    def index(self):
        return {'full_path': self.full_path}
