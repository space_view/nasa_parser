# Модуль для синхронизации контента с потребителями. Все фото из БД помещаются в rabbitMQ
import pymongo
from scrapy.utils.project import get_project_settings

from nasa_parser.amqp import connect, publish_message


def get_all_items(settings):
    client = pymongo.MongoClient(settings.get('MONGO_URL'))
    db = client[settings.get('MONGO_DATABASE')]
    return db[settings.get('MONGO_COLLECTION_NAME')].find({})


if __name__ == '__main__':
    project_settings = get_project_settings()
    queue_connection, queue_channel = connect(project_settings)
    for item in get_all_items(project_settings):
        item.pop('_id')
        publish_message(queue_channel, item)
